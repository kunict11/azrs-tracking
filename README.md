# azrs-tracking

Demonstracija primene alata za razvoj softvera na projektu [Circa2020](https://gitlab.com/kunict11/circa-2020).

Primenjeni alati:

1. Git
2. GDB
3. git hooks
4. Valgrind (Memcheck, Callgrind)
5. Clazy
6. Gammaray
7. GCov
8. Meld
9. Docker
10. GitLab CI/CD

Detaljnije na [Issue Boards](https://gitlab.com/kunict11/azrs-tracking/-/boards)
